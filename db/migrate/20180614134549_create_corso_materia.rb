class CreateCorsoMateria < ActiveRecord::Migration[5.2]
  def change
    create_table :corso_materia do |t|
      
      t.string :tipo
      
      t.timestamps
    end
  end
end
