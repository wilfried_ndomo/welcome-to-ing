require 'test_helper'

class VitaControllerTest < ActionDispatch::IntegrationTest
  test "should get alloggio" do
    get vita_alloggio_url
    assert_response :success
  end

  test "should get citta" do
    get vita_citta_url
    assert_response :success
  end

  test "should get viaggio" do
    get vita_viaggio_url
    assert_response :success
  end

  test "should get associazioni" do
    get vita_associazioni_url
    assert_response :success
  end

  test "should get divertimento" do
    get vita_divertimento_url
    assert_response :success
  end

end
