class AddSlogToReplies < ActiveRecord::Migration[5.2]
  def change
    add_column :replies, :slug, :string
  end
end
