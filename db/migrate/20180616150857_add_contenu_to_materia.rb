class AddContenuToMateria < ActiveRecord::Migration[5.2]
  def change
    add_column :materia, :contenuti_corso, :text
  end
end
