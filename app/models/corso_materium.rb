class MateriaTipoValidator < ActiveModel::Validator
  def validate(record)
    if record.tipo.eql? "A" or
       record.tipo.eql? "B" or#{}"Caraterizzante" or          
       record.tipo.eql? "C" or#{}"Affini" or                  
       record.tipo.eql? "D" or#{}"A scelta dello studente" or 
       record.tipo.eql? "E1" or #{}"Lingua straniera"  
       record.tipo.eql? ""         
      
    else
    	record.errors[:tipo] << "Deve essere: opzionale o obbligatoria"
    end
  end
end

class CorsoMaterium < ApplicationRecord
	belongs_to :dbcorso
  belongs_to :materium

	validates_with MateriaTipoValidator
  	validate :tipo
   
  	private

  	def dbcorso_unique_per_materium
    	if self.class.exists?(:dbcorso_id => dbcorso_id, :materium_id => materium_id,:tipo => tipo)
      		errors.add :dbcorso, 'already exists'
    	end
  	end

  	validate :dbcorso_unique_per_materium
  	
end
