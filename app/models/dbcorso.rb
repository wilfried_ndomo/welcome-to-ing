class Dbcorso < ApplicationRecord
	has_many :CorsoMaterium
    has_many :materia, :through => :CorsoMaterium

    validates :nome, :tipo, :descrizione, :sbocchi, :consigli, presence: true, uniqueness: false
    validates :nome, uniqueness: true
end
