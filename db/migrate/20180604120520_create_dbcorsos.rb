class CreateDbcorsos < ActiveRecord::Migration[5.2]
  def change
    create_table :dbcorsos do |t|
      
      t.string :nome
      t.string :tipo
      t.text :descrizione
      t.text :sbocchi
      t.text :consigli

      t.timestamps
    end
  end
end
