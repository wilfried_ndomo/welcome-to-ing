require "administrate/base_dashboard"

class DbcorsoDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    CorsoMaterium: Field::HasMany,
    materia: Field::HasMany,
    id: Field::Number,
    nome: Field::String,
    tipo: Field::String,
    descrizione: Field::Text,
    sbocchi: Field::Text,
    consigli: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    tag: Field::Text,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :CorsoMaterium,
    :materia,
    :id,
    :nome,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :CorsoMaterium,
    :materia,
    :id,
    :nome,
    :tipo,
    :descrizione,
    :sbocchi,
    :consigli,
    :created_at,
    :updated_at,
    :tag,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
  #  :CorsoMaterium,
   # :materia,
    :nome,
    :tipo,
    :descrizione,
    :sbocchi,
    :consigli,
    :tag,
  ].freeze

  # Overwrite this method to customize how dbcorsos are displayed
  # across all pages of the admin dashboard.
  #
   def display_resource(dbcorso)

    dbcorso.nome
  #   "Dbcorso ##{dbcorso.id}"
  end
end
