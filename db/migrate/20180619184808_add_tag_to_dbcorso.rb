class AddTagToDbcorso < ActiveRecord::Migration[5.2]
  def change
    add_column :dbcorsos, :tag, :text
  end
end
