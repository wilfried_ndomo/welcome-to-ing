class PagesController < ApplicationController
  
  def home
  end

  def corsi
    @interessi = params[:term]
    @sbocchi = params[:term2]
    require 'set'
    @corsis = Set[]
    #@for_tag = Set[]
    @for_tag = Dbcorso.all
    #INTERESSI  e SBOCCHI
     if params[:term] && params[:term2] && !params[:term].empty? && !params[:term2].empty?
    
          materie1 =  query_materia(params[:term])

          while materie1.size != 0 
              mat1 = materie1.first
              materie1.delete(mat1)
              for materia1 in mat1
                @corsis.add( Dbcorso.joins(:CorsoMaterium).where('materium_id = ? and
                                                              sbocchi LIKE ? or  
                                                              tag LIKE ? or
                                                              descrizione LIKE ?  or
                                                              consigli LIKE ? ',
                                                              "#{materia1.id}",
                                                              "%#{params[:term2]}%",
                                                              "%#{params[:term2]}%",
                                                              "%#{params[:term2]}%",
                                                              "%#{params[:term2]}%"

                                                              ).uniq)

              end
            end

          #INTERESSI 
          elsif params[:term] && params[:term2] && !params[:term].empty? && params[:term2].empty?
            materie =  query_materia(params[:term])

          #  render html: materie1.inspect
          
            while materie.size != 0 
              mat2 = materie.first
              materie.delete(mat2)
              for materia in mat2
                @corsis.add( Dbcorso.joins(:CorsoMaterium).where('materium_id = ? or
                                                                  nome LIKE ? or
                                                                  tag LIKE ?',
                                                                  "#{materia.id}",
                                                                  "%#{params[:term]}%",
                                                                  "%#{params[:term]}%").uniq)

              end
            end
           
          #SBOCCHI
          elsif params[:term] && params[:term2] && !params[:term2].empty? && params[:term].empty?
                
                @corsis.add(Dbcorso.where('sbocchi LIKE ? or 
                                           tag LIKE ?',
                                           "%#{params[:term2]}%",
                                           "%#{params[:term2]}%").uniq)
               

          #TUTTI I CORSI
          else
            if params[:tag]
              @corsis.add(Dbcorso.where('tag LIKE ?', "%#{params[:tag]}%"))
            else
               @corsis.add(Dbcorso.all)
            end
          end
  end

  def query_materia(arg)
    require 'set'
    mat = Set[]
    
    # cerchiamo prima le materie con tipo specificato che ordiniamo 
    mat.add( Materium.find_by_sql(["SELECT materia.id, corso_materia.id, materia.nome, corso_materia.tipo
                                      FROM materia JOIN corso_materia
                                      ON (materia.id = corso_materia.materium_id) 
                                      WHERE      nome LIKE ? or
                                                 obiettivi LIKE ? or
                                                 prerequisiti LIKE ? or
                                                 metodi_didattici LIKE ? or
                                                 madalita_verifica LIKE ? or
                                                 test_riferimento LIKE ? and
                                                 corso_materia.tipo != ?

                                      ORDER BY corso_materia.tipo ASC",

                                       "%#{arg}%",
                                       "%#{arg}%",
                                       "%#{arg}%",
                                       "%#{arg}%",
                                      "%#{arg}%",
                                       "%#{arg}%",
                                       ""
                                      ]))
      # poi in coda le materie con tipo non specificato
      mat.add( Materium.find_by_sql(["SELECT materia.id, corso_materia.id, materia.nome, corso_materia.tipo
                                      FROM materia JOIN corso_materia
                                      ON (materia.id = corso_materia.materium_id) 
                                      WHERE      nome LIKE ? or
                                                 obiettivi LIKE ? or
                                                 prerequisiti LIKE ? or
                                                 metodi_didattici LIKE ? or
                                                 madalita_verifica LIKE ? or
                                                 test_riferimento LIKE ? and
                                                 corso_materia.tipo = ?

                                      ORDER BY corso_materia.tipo ASC",

                                       "%#{arg}%",
                                       "%#{arg}%",
                                       "%#{arg}%",
                                       "%#{arg}%",
                                      "%#{arg}%",
                                       "%#{arg}%",
                                       ""
                                      ]))
  end


  def corso
    @corso = Dbcorso.find_by_id(params[:id])
    
    if @corso.nil?
      redirect_to root_path
    else
      # @mats = Materium.find_by_sql(["SELECT materia.id, corso_materia.id, materia.nome, corso_materia.tipo
      #                                FROM materia JOIN corso_materia
      #                                ON (materia.id = corso_materia.materium_id) 
      #                                WHERE corso_materia.dbcorso_id = ?
      #                                ", "#{@corso.id}"
      #                                ])
      @mats = Materium.find_by_sql(["SELECT materia.id, materia.nome, corso_materia.tipo
                                      FROM materia JOIN corso_materia
                                      ON (materia.id = corso_materia.materium_id) 
                                      WHERE corso_materia.dbcorso_id = ?

                                      ORDER BY materia.nome ASC",

                                       "#{@corso.id}"
                                      ])
    end
  end

  def materia

    @materia = Materium.find_by_id(params[:id])

    if @materia.nil?
      redirect_to root_path
    end
  end

  def dipartimento
  end

  def discussions
    
  end

end