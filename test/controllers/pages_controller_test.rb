require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get pages_home_url
    assert_response :success
  end

  test "should get corsi" do
    get pages_corsi_url
    assert_response :success
  end

  test "should get corso" do
    get pages_corso_url
    assert_response :success
  end

end
