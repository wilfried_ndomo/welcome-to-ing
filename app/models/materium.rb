class Materium < ApplicationRecord
	has_many :CorsoMaterium
    has_many :dbcorso, :through => :CorsoMaterium


    validates :nome, :docente, :obiettivi, :prerequisiti, :metodi_didattici,
              :madalita_verifica, :test_riferimento, presence: true, uniqueness: false
    validates :nome, uniqueness: false
end