require "administrate/base_dashboard"

class MateriumDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    CorsoMaterium: Field::HasMany,
    dbcorso: Field::HasMany,
    id: Field::Number,
    nome: Field::String,
    docente: Field::String,
    obiettivi: Field::Text,
    prerequisiti: Field::Text,
    metodi_didattici: Field::Text,
    madalita_verifica: Field::Text,
    test_riferimento: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    contenuti_corso: Field::Text,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :CorsoMaterium,
    :dbcorso,
    :id,
    :nome,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :CorsoMaterium,
    :dbcorso,
    :id,
    :nome,
    :docente,
    :obiettivi,
    :prerequisiti,
    :metodi_didattici,
    :madalita_verifica,
    :test_riferimento,
    :created_at,
    :updated_at,
    :contenuti_corso,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
  #  :CorsoMaterium,
  #  :dbcorso,
    :nome,
    :docente,
    :obiettivi,
    :prerequisiti,
    :contenuti_corso,
    :metodi_didattici,
    :madalita_verifica,
    :test_riferimento,
    
  ].freeze

  # Overwrite this method to customize how materia are displayed
  # across all pages of the admin dashboard.
  #
   def display_resource(materium)
    materium.nome
  #   "Materium ##{materium.id}"
   end
end
