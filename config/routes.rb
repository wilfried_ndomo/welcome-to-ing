Rails.application.routes.draw do
  resources :channels
  resources :discussions
  #devise_for :users
  namespace :admin do
      resources :dbcorsos, :materia, :corso_materia


      root to: "dbcorsos#index"
    end
  root 'pages#home'
  get  'vita/alloggio'     => 'vita#alloggio'
  get  'vita/citta'        => 'vita#citta'
  get  'vita/viaggio'      => 'vita#viaggio'
  get  'vita/associazioni' => 'vita#associazioni'
  get  'vita/divertimento' => 'vita#divertimento'
  get  'corsi'             => 'pages#corsi'
  get  'corsi/corso/:id'   => 'pages#corso'
  get  'tags/corsi/corso/:id'   => 'pages#corso'
  get  'dipartimento'             => 'pages#dipartimento'
  get  'materia/:id' => 'pages#materia'
  get  'tags/:tag', to: 'pages#corsi', as: :tag
  get  'discussions' => 'discussions#index'

  resources :channels
  resources :discussions do
    resources :replies
  end


  devise_for :users, controllers: { registrations: 'registrations' }

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
