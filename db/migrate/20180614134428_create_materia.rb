class CreateMateria < ActiveRecord::Migration[5.2]
  def change
    create_table :materia do |t|
      
      t.string :nome
      t.string :docente
      t.text :obiettivi
      t.text :prerequisiti
      t.text :metodi_didattici
      t.text :madalita_verifica
      t.text :test_riferimento

      t.timestamps
    end
  end
end
